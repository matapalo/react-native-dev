import {    
    createStackNavigator,
    createSwitchNavigator,
    createAppContainer,
} from 'react-navigation';

import Login from './src/screens/Login';
import Home from './src/screens/Home';

import Initializing from './src/screens/Initializing';
import Drawer from './src/screens/Drawer';
import Dashboard from './src/components/Dashboard';


/**
 * Stack (pino) tarkoittaa, että käyttäjä voi liikkua ikkunoiden
 * välillä. Esim. Login-sivulta pääsee takaisin Home-ikkunaan.
 * 
 */

const MainNavigator = createStackNavigator({
        Home: {screen: Home},
        Drawer: {screen: Drawer},
        Login: {screen: Login}
    },
    {
        initialRouteName: 'Home',
    }
);


/**
 * Dashboard on oma komponenttinsa, joka määrittelee vetovalikon
 */
const ProfileStack = createStackNavigator({
        MainDrawer: Dashboard
    },
    {
        initialRouteName: 'MainDrawer',
    }
);

/**
 * AppContainer on koko sovelluksen "kontti".
 * Se sisältää SwitchNavigaattorin, joka puolestaan
 * sisältää StackNavigaattorit. Sovelluksen ensimmäinen
 * ikkuna on Initializing.
 * 
 * SwitchNavigator:
 * Mikäli käyttäjä on jo kirjautunut initializing-screenin
 * jälkeen käyttäjä päätyy ProfileStack:iin. Muutoin käyttäjä
 * ohjataan MainNavigator-Stack:iin. Ohjauksen jälkeen ei enää
 * pääse takaisin sinne mistä tuli.
 *  
 * MainNavigatorissa pääsee Sign In -ikkunaan.
 */

export default createAppContainer(createSwitchNavigator(
    {
        Initializing: {screen: Initializing},
        App: MainNavigator,
        Profile: ProfileStack
    },
    {
        initialRouteName: 'Initializing'
    }
));