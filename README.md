# Inspire - Mobile App

> VAROITUS: näiden ohjeiden seuraaminen on omalla vastuulla! :laughing:

Kannattaa lukea netistä ohjeita ja valmista React-koodia. Seuraava linkki vie ohjeeseen react-nativen asennuksesta ja oman projektin aloituksesta. Lue sivulta React Native CLI quickstart ja omaan käyttöjärjestelmään sopiva ohje.

[React-Nativen ohje](https://facebook.github.io/react-native/docs/getting-started.html)

## Huomioita

Projektia olen työstänyt Linux Ubuntu 18.04 LTS käyttiksellä. VSCode on hyvä editori näihin hommiin. Lisäosiksi editoriin voi dokumentointia varten asentaa markdownlint ja PlantUML.

Android-Studio editoria voi käyttää esim. emulaattorin päivittämiseen ja Gradlen hallintaan. Kommentointia olen javascript-tiedostoihin lisäillyt JSDoc:ina.

Projektia muokkasin niin, että serveriin liittyvä koodi on poistettu tästä versiosta. Tämä on ikään kuin boilerplate-versio. Backend-puoli koostuu osista: Express - Node - Sequeline - postgreSQL.

Emulaattorin ollessa päällä, sen valikon saa auki Ubuntulla: Ctrl+M. Sieltä voi klikata vaikka "reload" tai "enable hot reloading". Emulaattori näyttää virheitä, jos projektin simulointi ei onnistu. Koodissa on yleensä jotain puutteita, mutta emulaattorin virheilmoitukset saattavat olla melko kryptisiä. Itse en ole käyttänyt mitään debug-työkaluja tai vastaavia. Niitä löytyy varmasti, mutta olen saanut ratkaistua ongelmat aina ilmankin. Enemmän virheitä tuli siinä kohtaa, kun kirjoitin vielä react-native-komponentteja. Nyt kun vaihdoin nativebase-komponentteihin niin homma on helpottunut.

Tässä versiossa emulaattori syöltää aika paljon varoituksia. Niihin voi etsiä ratkaisuja sitten myöhemmin. Pääasia on, että sovellus toimii eikä tule virheitä.

## Gitlab-projekti

1. Kloonaa projekti komennolla "git clone".
2. Siirry komentorivillä source-kansioon
3. Aja komento "npm install"
4. Aja komento "react-native eject"
5. Aja komento "react-native run-android"

Emulaattori ei ole käynnissä, jos tulee virhe:

```shell
> Task :app:installDebug FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':app:installDebug'.
> com.android.builder.testing.api.DeviceException: No connected devices!
```

Käynnistä emulaattori taustalle! Aja uudelleen komento "react-native run-android".

Seuraava virhe pitäisi näkyä emulaattorissa:

```shell
> null is not an object (evaluating 'rngesturehandlermodule.state')
```

Aja komento (pysäytä build aina ennen link-komentoa):

```shell
react-native link react-native-gesture-handler
```

Nyt pitäisi toimia, mutta emulaattorissa ikonit ei näy oikein.

Aja komento:

```shell
react-native link react-native-vector-icons
```

Nyt pitäisi näkyä ikonitkin.

## Projektin kansiorakenne

Tekemäni tiedostot ovat pääasiassa projektin src-kansiossa (loin itse src-kansion javascript-tiedostoille). Sieltä löytyy javascript-tiedostot komponenteille yms. Olen myös muokannut tiedostoja App.js, index.js ja Routes.js. Nämä löytyy projekti-kansiosta (eli Inspire-kansiosta). Niin kuin nimestä jo selviää, Routes.js sisältää navigointiin liittyviä määrityksiä. App.js sisältää päätason javascriptin ja Index.js liittää koodin native-moduuleihin. Näistä ei tarvitse sen enempää välittää tässä vaiheessa, ja index.js tiedostoon ei sitten kosketa missään vaiheessa! Tiedostoa Routes.js sen sijaan tulee muokata, jos haluat lisätä uusia ikkunoita sovellukseen.

> Kannattaa käydä tekemiäni js-tiedostoja läpi, koska niissä on kommentointia.

Muut tiedostot (mm. xml, java ja gradle) luodaan valmiiksi, kun aloitat oman projektin. Niihin ei kannata koskea. Joissakin tapauksissa valmiiksi luotuja java-tiedostoja on muokattava, jotta jokin uusi asennettu moduuli toimisi oikein (npm install). Näihin hommiin löytyy asennettavan moduulin webbisivulta ohjeet, joten ei hätää.

Projektin res-kansiosta (/src/res) löytyy Images-kansio. Sinne sitten omat logot ja kuvat. Esim. tiedostossa Home.js viittaus koodissa kuvaan tapahtuu seuraavasti:

```javascript
<Image source={require('../res/Images/Icons/InspireNetworks.png')} style={{width: 33, height: 33, marginRight: 16 }}/>
```

Eli Home.js tiedostoon nähden kuva sijaitsee relatiivisessa-polussa:

> ../res/Images/Icons/InspireNetworks.png).

Tämä siis react-nativen Image-komponentilla (jonka jo poistin). Nativebase:lla saa ainakin valmiita ikoneja (Icon).

## Navigointi

Navigointia voi olla vaikea hahmottaa koodista (tai koodin kommenteista), joten seuraavassa kuvia avuksi. Ei ehkä siltikään aukea, mutta netistä voi lukea lisäinfoa.

> Gitlab:in pitäisi ymmärtää plantuml-muotoja.

Kuvassa Drawer on sivuvalikko (tämä kuva ei näy Gitlab:ssa, mutta näkyy VSCode:lla - plantUML lisäosa asennettuna).

```plantuml
@startmindmap
* Init
** Home
*** Try the app
*** Sign In
**** Drawer
***** Profile
***** Home
***** Connect
***** Init
@endmindmap
```

Tässä vaiheessa vielä vetovalikosta (Drawer) pääsee home/init-ikkunaan ja sitä kautta uudelleen kirjautumisikkunaan. *Navigointia pitää/voi muuttaa!* 

Seuraava kuva voi selventää Switch-navigointia. Mikäli käyttäjä on kirjautunut, käyttäjä ohjataan omaan profiiliin, jossa on vetovalikko (navigation drawer). Drawer- ja profiili-ikkunat ovat Stack:ssa eli "pinossa". Termit "Switch" ja "Stack" toistuvat sovelluksen koodissa (Routes.js). Myös "Try the app"- ja "Sign In"-ikkunat ovat Stack:ssa.

```plantuml
@startuml

skinparam monochrome true

(*) --> "Initialization"
if "Logged In?" then
    -->[true] "Drawer"
    --> "Profile"
else
    -->[false] "Try the app"
    --> "Sign In"
    --> "Drawer"
endif
@enduml
```

## Sovelluksen ajaminen

Emulaattori käyntiin taustalle.
Sovellus ajetaan komentoriviltä projekti-kansiosta (eli Inspire-kansiosta) seuraavasti:

```shell
react-native run-android
```

Komennon jälkeen Gradle-build käynnistyy. Toivottavasti emulaattorissa alkaa näkymään jotain. Mikäli emulaattori ei näytä koodinmuutoksia oikein niin aja gradle-build uudelleen.

## Sovelluksen toiminta

Sovelluksen tämä versio on siinä vaiheessa, että kirjautumissivulta pääsee "SIGN IN" -nappia painamalla eteenpäin. Ei tarvitse kirjoittaa (ylälaidan) kenttiin mitään.

Sovelluksen ikkunoiden ulkoasu on miten sattuu, mutta en ole siihen kiinnittänytkään huomiota.

"Sign In" -ikkunan alemmat kirjautumissivun react-nativen Input-komponentit "muistavat" käyttäjän tiedot, mutta ylemmät nativebase-pohjaiset Input-komponentit ei muista tietoja. Kokeile niin näet! Kirjautumiseen riittää, kun "Full Name" kenttään antaa yhden merkin (salasanaa ei tarvita).

> Voit tietysti poistaa kaikki react-nativen komponentit ikkunoista ja vaihtaa nativebasen vastaaviin.

Kirjautumistiedot voit nollata tiedoston Initializing.js (tiedosto löytyy polusta ./src/screens/) kohdasta:

```javascript
    async componentDidMount() {
        //await AsyncStorage.clear();
```

Kommenttimerkit // pois, niin storage tyhjenee (tietty tallennus ja emulaattorin reload tähän päälle).

## Node ja npm

Tiedosto package.json sisältää kaikki asennetut moduulit ja riippuvuuksien kuvaukset. Työkalulla npm asennetaan projektissa käytettävät moduulit. Seuraava komento luo tiedoston package.json

```shell
npm init
```

Tämä ajetaan omaa projektia aloitettaessa. Node.js on oltava asennettuna, koska npm asentuu Noden mukana.

Esimerkiksi native-base on asennettu seuraavasti:

```shell
npm i -D native-base
```

Vastaava kuin seuraava komento (tätä näkee usein käytettävän):

```shell
npm install --save native-base
```

Asennettu moduuli näkyy tiedostossa package.json kohdassa "devDependencies" (development). Asennettu moduuli näkyy tiedostossa package.json kohdassa "dependencies" (production), mikäli asennat näin:

```shell
npm install native-base
```

Sitten vaan js-tiedostoon importataan moduuli:

```javascript
import { Button } from 'native-base';
```

Nyt voit käyttää Button-komponenttia sovelluksessa. Komponentin voi importata myös vapaavalintaisella nimellä:

```javascript
import { Button as omaButton } from 'native-base';
```

Tätä voi hyödyntää tilanteessa, jossa on useampi moduuli käytössä ja kahdesta importataan samanniminen komponentti Button. Ainakin toisen nimi tulee muttaa importissa. Koodissa komponentin nimenmuutos näkyy seuraavasti:

```javascript
<Button></Button>
```

> Button --> omaButton

```javascript
<omaButton></omaButton>
```

## Loppukevennykset

### Serveri + Sovellus (tällä hetkellä)

```plantuml
@startuml
node "Node Sovellus" {
    [Sequelize]
    [Node.js]
    [Express]
}

node "Sovellus" {
    [POST/GET]
    [Response]
}


database "DB" {
    folder "postgreSQL" {
        [Table]
    }
}

[POST/GET] --> [Express]
[Express] --> [Response]
[Table] <--> [Sequelize] 
@enduml
```

### Hahmotelma projektista (mahdollinen)

```plantuml
@startuml component

package "GitLab Project" {
    [Source]
    [Wiki]
    [Issues]
}
database "postgresSql" {
    folder "database" {
        [data]
    }
}
package "CI/CD" {
    folder "pipeline"{
        [Jenkins]
        [robot framework]
    }
}
package "Docker Registry" {
    folder "Docker" {
        [App]
    }
}
[Source] --> [Jenkins]
[robot framework] --> [Jenkins]
[Jenkins] --> [App]
[App] <--> [data]

note right of [Jenkins]
  Robot framework --> testit lähdekoodille 
  Jenkins plugin --> robot framework-raporttien lukemiseen,
  Jenkins xvfb plugin --> headless
end note
note right of [App]
  Docker-kontissa: Gradle + Java + Android Studio
end note
@enduml
```

### Käyttötapaukset

> Käyttötapauksia voisi kuvata jotenkin näin:

```plantuml
@startuml
left to right direction
skinparam packageStyle rectangle
skinparam defaultFontColor grey
actor OldUser
actor NewUser
actor OldUser2
actor Business
actor Admin
rectangle MobileApp_GeneralUseCase {
    (Make/organize connections) -- Business
    NewUser -- (Register)
    NewUser -- (Try the app)
    NewUser -- (Provide feedback)
    OldUser -- (Sign in)
    OldUser -- (Provide feedback)
    OldUser -- (Make/organize connections)
    OldUser -- (Chatting)
    (Chatting) -- OldUser2
    OldUser -- (Search)
    OldUser -- (Edit Profile)
    (Provide feedback) -- Admin
}
@enduml
```

#### Käyttötapaus: kirjautuminen

> Valitaan yksi käyttötapaus lähempään tarkasteluun (näin esimerkiksi).

```plantuml
@startuml
left to right direction
skinparam packageStyle rectangle
skinparam defaultFontColor grey
actor OldUser
rectangle Login_to_MobileApp {
    OldUser -- (Log in)
    (Verify Password) .> (Log in) : include
    (Display login error) .> (Log in) : extend
}
note right of (Verify Password)
  include: tapahtuu joka kerta
end note
note right of (Display login error)
  extend: mahdollisuus tapahtua
end note
@enduml
```

> Käyttötapaukset täytyisi avata vielä sanallisesti vaihe vaiheelta. Mitä tapahtuu esimerkiksi käyttäjän kirjautuessa.