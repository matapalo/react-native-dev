import React, {Component} from 'react';
import { View } from 'react-native';
import { Avatar } from 'react-native-elements';


/**
 * Tässä ikkunassa ei ole vielä nativebasea käytössä
 */
export default class Profile extends Component {

    static navigationOptions = {
        title: 'Profile'
    }

    render() {
        return (
            <View>
                <Avatar rounded icon={{ name: "user", type: 'font-awesome' }}
                size="large"
                containerStyle={{ marginTop: 15, marginLeft: 15 }}
                showEditButton/>
            </View>
        );
    }
}