import React, {Component} from 'react';
import { Container, Header, Item, Input, Icon, Button, Text } from 'native-base';


/**
 * Valmis nativebase-komponentti
 */
export default class Connect extends Component {

    state = {
        search: '',
    };

    // tilan muutos tehdään aina setState-metodilla
    updateSearch = search => {
        this.setState({ search });
    };

    render() {

        return (
            <Container>
                <Header searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search" />
                        <Icon name="ios-people" />
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
            </Container>
        );
    }
}
