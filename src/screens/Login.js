import React, {Component} from 'react';
// jos on pitkä import-list niin tätä muotoa on mielestäni helpompi lukea
import { 
    Button, 
    Container, 
    Content, 
    Form, 
    Item, 
    Input, 
    Label, 
    Text 
} from 'native-base';
import AsyncStorageEx from '../storage/userStorage';

export default class Login extends Component {

    static navigationOptions = {
        title: 'Home'
    }

    // komponentin tilan alustus
    constructor(props){
    super(props);
        this.state = {
            email: null,
            password: null,
        }
    }

  render() {

    /**
     * Input-komponentin accessibilityLabel on testejä varten (robot-framework),
     * jotta komponenttiin on helppo viitata testejä kirjoitettaessa.
     * 
     * Käyttäjä päätyy Profiles Stack:iin, kun klikkaa Button:ia (Sign In).
     * AsyncStorageEx on oma komponentti, joka sisältää toiset Input-komponentit.
     * Näihin syötetyt tiedot sovellus "muistaa".
     */

    return (
        <Container>
            <Content>
                <Form >
                    <Item stackedLabel success>
                        <Label>Email</Label>
                        <Input onChangeText={value => this.setState({email: value})} 
                               accessibilityLabel='FullNameField'         
                        />
                    </Item>
                    <Item stackedLabel last>
                        <Label>Password</Label>
                        <Input  onChangeText={value => this.setState({password: value})} 
                                accessibilityLabel='PasswordField'
                                secureTextEntry={true}
                        />
                    </Item>                   
                </Form>
                <Button block
                    onPress={() => this.props.navigation.navigate('Profile')}
                >
                    <Text>Sign In</Text>
                </Button>
                <AsyncStorageEx />    
            </Content>
        </Container>
    );
  }
}

