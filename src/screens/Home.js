import React, {Component} from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Button, Icon, Text } from 'native-base';


/**
 * Luokka koko Home-ikkunalle
 * 
 * export default voidaan importata muualla (jossakin toisessa js-tiedostossa)
 */
export default class Home extends Component {

    /**
     * Esimerkkinä Home-luokan change-metodi
     * metodia käytin yksikkötestuksessa
     * testi löytyy Inspire-kansion polusta __tests__
     * kansiossa on tiedosto on Function-test.js
     * voit katsoa miten yksikkötesti kirjoitetaan
     * 
     * metodi ei tee mitään järkevää
     * palauttaa kaksikertaa suuremman luvun
     * 
     * huom. JavaScriptissä ei ole tyyppejä, esim. integer tai string
     * muuttujat esitellään sanoilla "var" tai "let"
     * 
     * @param {int} x - parametrina kokonaisluku x  
     */
    change(x){
        return x * 2;
    }

    /**
     * Ikkunan Header-osa
     * NativeBasen omena-ikoni ikkunan oikeassa laidassa
     * css-tyylillä saadaan siirrettyä kuvaa keskemmälle (marginRight)
     */
    static navigationOptions = () => ({
        title: 'Inspire',
        headerRight: (
            <Icon name='logo-apple' style={{ marginRight: 5 }}/>
        )
    });


    /** 
     * render on luokan Component metodi, joka on peritty luokkaan Home (Home extends Component)
    */
    render() {

        /** 
         * return-osaan lisätään komponentit
         * nativebase.io-sivustolta vaan copy+paste
         * 
        */
        return (
            <Container>
            <Content>
              <Button iconLeft 
                      primary
                      block
                      onPress={() => this.props.navigation.navigate('Login')}
                      style={styles.ButtonStyle}
              >
                <Icon name='arrow-forward' />
                <Text>Sign In</Text>
              </Button>
              <Button iconLeft 
                      primary
                      block
                      onPress={() => this.props.navigation.navigate('Drawer')}
                      style={styles.ButtonStyle}
              >
                <Icon name='swap' />
                <Text>Try the app</Text>
              </Button>
            </Content>
          </Container>            
        );
    }
}

/**
 * Asetetaan Button:lle vähän tyhjää tilaa yläpuolelle
 * Muuta arvoa 5 ja katso mitä tapahtuu
 */
const styles = StyleSheet.create({
    ButtonStyle: {
        marginTop: 5,
    }
});