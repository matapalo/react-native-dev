
import React, {Component} from 'react';
// mielestäni tätä import-tyyliä on helpompi lukea eli kaikki ei samalla rivillä
import {
    Button,
    Container,
    Content,
    Icon,
    Text,
} from 'native-base';

/**
 * Tämä komponentti ei ole drawer.
 * Nimen voi muuttaa.
 */
export default class Drawer extends Component {

    render(){
        return (
            <Container>
                <Content padder>
                  <Button
                    onPress={() => this.props.navigation.navigate('Home')}
                    iconLeft
                    block
                    primary>
                    <Icon name='home'/>
                    <Text>Click Me!</Text>
                  </Button>
                </Content>
            </Container>
        )
    }
}