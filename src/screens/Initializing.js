import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  AsyncStorage,
  YellowBox
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';


/**
 * Tällä ikkunalla ohjataan käyttäjää Switch-navigaattorilla.
 * 
 * export default --> voidaan importata muualla
 */
export default class Initializing extends Component {

    /**
     * componentDidMount() suoritetaan heti, kun komponentti on alustettu.
     * 
     * Mikäli haluat sovelluksen unohtavan käyttäjän tiedot, niin 
     * poista kommentti kohdasta: await AsyncStorage.clear();
     * tallenna tiedosto ja emulaattorille reload (Ubuntulla Ctrl+M ja klikkaa reload).
     * 
     */
    async componentDidMount() {
        //await AsyncStorage.clear();
        const user = await AsyncStorage.getItem('@MySuperStore:key');

        /**
         *  Tässä kohtaa tapahtuu switch-navigointi.
         *  Siirrytään 'Profile'-Stack:iin, jos käyttäjästä on tieto muistissa.
         *  Muulloin siirrytään 'App'-Stack:iin.
        */
        try {
            this.props.navigation.navigate(user ? 'Profile' : 'App')
        } catch (err) {
            this.props.navigation.navigate('App');
        }
    }

    render() {

        // emulaattorin ärsyttäviä (mutta joissain tapauksissa hyödyllisiä) varoituksia voi poistaa näin
        YellowBox.ignoreWarnings(['Warning: Async Storage has been extracted from react-native core']);

        /**
         * Splash-ikkunalla on vain muutama komponentti
         * Icon-komponentti olisi hyvä saada animoiduksi jossain kohtaa
        */
         return (
          <View style={styles.container}>
            <Icon name='spinner' size={35} />
            <Text style={styles.welcome}>Loading...</Text>
          </View>
        )
    }
}

/**
 *  CSS-tyylit komponenteille (flexbox käytössä)
 *  Nämä voisi siirtää omaan tiedostoon erilleen komponenteista ja ikkunoista
 */
const styles = StyleSheet.create({
  welcome: {
    fontSize: 28
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})