import React, { Component } from 'react';
import {
    AsyncStorage,
    View,
    StyleSheet,
    Dimensions
} from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


// ikkunan leveys muuttujaan
const { width: WIDTH} = Dimensions.get('window');

/**
 * "Local Storage" muistaa kirjautumistiedot.
 * 
 *  Input-komponentin accessibilityLabel on testejä varten (robot-framework),
 *  jotta komponenttiin on helppo viitata testejä kirjoitettaessa. 
 */

export default class AsyncStorageEx extends Component {


    /**
     * Tilallinen komponentti
     * Voidaan asettaa käyttäjän nimi
     * 
     * @param {*} props 
     */
    constructor(props){
        super(props);
        this.state = {
            name: null
        }
    }

    /**
     * Asettaa avain:arvo -parin muistiin
     * eli username: 'nimesi'
     * 
     * @param {string} username 
     */
    async setKey(username) {
        try {
            await AsyncStorage.setItem('@MySuperStore:key', username)
            console.log(username);
        }
        catch(error) {
            console.log(error.message);
        }
    };

    render() {
        return (
             <View style={{ marginTop: 10 }}>
                <Input
                    placeholder='Full Name'
                    inputContainerStyle={styles.textInput}
                    leftIcon={
                        <Icon
                            name='user'
                            size={24}
                            color='black'
                        />
                    }
                    accessibilityLabel='FullNameField'
                    value={this.state.name}
                    onChangeText={(value) => this.setKey(value) }
                />
                <Input
                    placeholder='Password'
                    inputContainerStyle={styles.textInput}
                    leftIcon={
                        <Icon
                            name='lock'
                            size={24}
                            color='black'
                        />
                    }
                    accessibilityLabel='PasswordField'
                    secureTextEntry={true}
                    value={this.state.name}
                    onChangeText={(value) => this.setKey(value) }
                />
              </View>
        )
    }
}

/**
 * Komponenttien tyylit
 */
const styles = StyleSheet.create({
    textInput: {
        borderColor: "gray",
        backgroundColor: '#ffffff',
        borderWidth: 1,
        color: "blue",
        height: 40,
        fontSize: 16,
        width: WIDTH - 18,
        marginBottom: 5,
        borderRadius: 18,
        textAlign: 'center'
    },
    formButton: {
        borderWidth: 1,
        borderColor: '#555555'
    }
})
