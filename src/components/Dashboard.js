
import React from 'react';
import { View } from 'react-native';
import { Button } from 'react-native-elements';
import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';


// sovelluksen ikkunoita importataan screens-kansiosta
import Home from '../screens/Home';
import Connect from '../screens/Connect';
import Profile from '../screens/Profile';
import Initializing from '../screens/Initializing';


/**
 * Tämä funktio tuottaa komponentin, jota klikkaamalla
 * avautuu vetovalikko
 * 
 * @param {*} navigation 
 */
const renderMenu = (navigation) => (
    <View>
        <Button
            icon={
                <Icon
                    name='bars'
                    size={25}
                    color='white'
                />
            }
            buttonStyle={{ marginRight: 6 }}
            onPress={ () => navigation.openDrawer() }
        />
    </View> 
)

/**
 * Kaikki seuraavat Stack-ikkunat lisätään vetovalikkoon
 * esim. importattu Home-komponentti laitetaan kohtaan screen: Home
 */
const HomeScreen = createStackNavigator({
    HomeScreen: {
        screen: Home,
        navigationOptions: ({navigation}) => ({
            title: 'Home',
            headerRight: renderMenu(navigation)
        })
    }
});

const ConnectScreen = createStackNavigator({
    ConnectScreen: {
        screen: Connect,
        navigationOptions: ({navigation}) => ({
            title: 'Connect',
            headerRight: renderMenu(navigation),
            hearder: null
        })
    }
});

const InitScreen = createStackNavigator({
    InitScreen: {
        screen: Initializing,
        navigationOptions: ({navigation}) => ({
            title: 'Init',
            headerRight: renderMenu(navigation)
        })
    }
});

const ProfileScreen = createStackNavigator({
    ProfileScreen: {
        screen: Profile,
        navigationOptions: ({navigation}) => ({
            title: 'Profile',
            headerRight: renderMenu(navigation)
        })
    }
});


/**
 * Vetovalikko luodaan tässä
 * vaihda drawerPosition: 'right' ja valikko aukeaa oikealta
 */
const Dashboard = createDrawerNavigator(
    {
        Profile: {
            screen: ProfileScreen,
            navigationOptions: {
                drawerLabel: 'Profile',
                drawerIcon: (
                    <Icon name='user' size={25} />
                )
            }
        },
        Connect: {
            screen: ConnectScreen,
            navigationOptions: {
                drawerLabel: 'Connect',
                drawerIcon: (
                    <Icon name='plus-circle' size={25} />
                )
            }
        },
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                drawerLabel: 'Home',
                drawerIcon: (
                    <Icon name='home' size={25} />
                )
            }
        },
        Init: {
            screen: InitScreen,
            navigationOptions: {
                drawerLabel: 'Init'
            }
        }
    },
    {
        drawerPosition: 'left',
    }
);

/**
 * export default --> voidaan importata muualla
 */
export default Dashboard;