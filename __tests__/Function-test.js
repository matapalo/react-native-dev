
import 'react-native';
import React from 'react';

import renderer from 'react-test-renderer';

import Home from '../src/screens/Home';

/**
 * Yksikkötesti Home-komponentin change-metodille
 * Kaksinkertaistaa syötetyn arvon eli change(2) --> 2 * 2 = 4 --> toEqual(4)
 * Testit ajetaan komentoriviltä komennolla:
 * npm test
 */
it('Function and state test care', () => {
    let HomeData = renderer.create(<Home />).getInstance();

    expect(HomeData.change(2)).toEqual(4);

});